import "./Styles/App.css";
import Global from "./Global";
import Todo from "./Components/Todo";
import { MouseParallaxContainer, MouseParallaxChild } from "react-parallax-mouse";
import Waves from "./Components/Waves";
import ParallaxCircles from "./Components/ParallaxCircles";
import image from "./Assets/white_circle.png";

function App() {
  return (
    <>
      <div className='app layer'>
        <MouseParallaxContainer className='parallax-container'>
          <MouseParallaxChild factorX={0.04} factorY={0.07}>
            <p className='watermark'>Created by: Aleksandar Svetoslavov</p>
          </MouseParallaxChild>
          <MouseParallaxChild factorX={0.03} factorY={0.05} className='parallax-child'>
            <div className='app-container layer'>
              <Global Root={() => <Todo />} />
            </div>
          </MouseParallaxChild>
          <MouseParallaxChild factorX={0.17} factorY={0.18}>
            <ParallaxCircles />
          </MouseParallaxChild>
          <MouseParallaxChild factorX={0.27} factorY={0.28}>
            <img src={image} alt='' className='layer-2' />
          </MouseParallaxChild>
        </MouseParallaxContainer>
      </div>
      <Waves />
    </>
  );
}

export default App;
