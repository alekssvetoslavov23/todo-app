import React from "react";
import { Zoom, Checkbox, Tooltip, ListItemText, ListItemButton, ListItem } from "@mui/material";
import editIcon from "../Assets/edit-icon.svg";
import deleteIcon from "../Assets/delete-icon.svg";
class Task extends React.Component {
  render() {
    const { task, finishTask, index, labelId, editTask, deleteTask } = this.props;
    return (
      <ListItem disablePadding className='task-list-item'>
        <ListItemButton role={undefined} dense>
          <ListItemText primary={`${index}. `} className={task.done ? "task-text done" : "task-text"} />
          <Tooltip
            title={task.name}
            TransitionComponent={Zoom}
            TransitionProps={{ timeout: 350 }}
            placement='top-start'
            className='task-tooltip'
            arrow>
            <ListItemText primary={`${task.name}`} className={task.done ? "task-text done" : "task-text"} />
          </Tooltip>
        </ListItemButton>
        <Tooltip
          title={"Mark task as finished."}
          TransitionComponent={Zoom}
          TransitionProps={{ timeout: 350 }}
          placement='top'
          className='task-tooltip'
          arrow>
          <Checkbox
            edge='end'
            onChange={() => {
              finishTask(task);
            }}
            tabIndex={-1}
            checked={task.done}
            disableRipple
            inputProps={{ aria_labelledby: labelId }}
            disabled={task.done}
          />
        </Tooltip>
        {task.done ? null : (
          <>
            <Tooltip
              title={"Edit task."}
              TransitionComponent={Zoom}
              TransitionProps={{ timeout: 350 }}
              placement='top'
              className='task-tooltip'
              arrow>
              <img src={editIcon} alt={"Edit"} className='edit-icon' onClick={() => editTask(task)} />
            </Tooltip>
            <Tooltip
              title={"Delete task."}
              TransitionComponent={Zoom}
              TransitionProps={{ timeout: 350 }}
              placement='top'
              className='task-tooltip'
              arrow>
              <img src={deleteIcon} alt={"Delete"} className='delete-icon' onClick={() => deleteTask(task)} />
            </Tooltip>
          </>
        )}
      </ListItem>
    );
  }
}

export default Task;
