import { GlobalState, useGlobalState } from "../Global";
import { Container, Tab, Alert, Collapse, List } from "@mui/material";
import { TabPanel, TabContext, TabList } from "@mui/lab";

import Task from "./Task";
import "../Styles/App.css";
import InputWrap from "./InputWrap";

function Todo() {
  let { tasks, currentTask, currentTab, openAlert, typeAlert, textAlert, editTaskName, dialogDelete, dialogOpen } = useGlobalState();

  //  Setting default values to the global states
  if (tasks === undefined) tasks = [];
  if (currentTask === undefined) currentTask = "";
  if (currentTab === undefined) currentTab = "todo";
  if (openAlert === undefined) openAlert = false;
  if (typeAlert === undefined) typeAlert = "error";
  if (textAlert === undefined) textAlert = "";
  if (editTaskName === undefined) editTaskName = "";
  if (dialogOpen === undefined) dialogOpen = false;
  if (dialogDelete === undefined) dialogDelete = false;

  // Adding a task. The name of the task is got from the input and the global state is updated
  // If nothing was entered, error alert will popup
  let addTask = (task) => {
    // If nothing entered
    if (task === "") {
      GlobalState.set({
        tasks: tasks,
        openAlert: true,
        typeAlert: "error",
        textAlert: "You must enter a task first!",
      });

      // Close after 2 seconds
      setTimeout(() => {
        GlobalState.set({
          openAlert: false,
        });
      }, 2000);
    } else if (tasks.filter((e) => e.name === task).length > 0) {
      GlobalState.set({
        tasks: tasks,
        openAlert: true,
        typeAlert: "error",
        textAlert: "Such a task is already added!",
      });

      // Close after 2 seconds
      setTimeout(() => {
        GlobalState.set({
          openAlert: false,
        });
      }, 4000);
    } else {
      // If editTaskName contains string, it means that there is a task selected for edit
      // That means we want to update the task name instead of add a new task
      if (editTaskName !== "") {
        let tasksUpdated = [...tasks].filter((todo) => {
          if (todo.name === editTaskName) {
            todo.name = currentTask;
          }
          return todo;
        });

        GlobalState.set({
          tasks: tasksUpdated,
          currentTask: "",
          editTaskName: "",
          openAlert: true,
          typeAlert: "success",
          textAlert: "Task edited successfully!",
        });
        setTimeout(() => {
          GlobalState.set({
            openAlert: false,
          });
        }, 4000);
      } else {
        // Otherwise, iff the user entered a task, task object is created and added to a list, stored in the global state
        let task_object = {
          name: task,
          done: false,
        };
        tasks.push(task_object);

        GlobalState.set({
          tasks: tasks,
          currentTask: "",
        });
      }
    }
  };
  // It will set the editTaskName to the name of the task that needs to be edited
  // Then in addTask instead of adding a new task, the focused one will be edited
  let editTask = (task) => {
    let taskName = task.name;
    GlobalState.set({
      currentTask: taskName,
      editTaskName: taskName,
    });
  };

  let deleteTask = (task) => {
    let tasksUpdated = [...tasks].filter((todo) => todo.name !== task.name);
    GlobalState.set({
      tasks: tasksUpdated,
      openAlert: true,
      typeAlert: "warning",
      textAlert: "Task deleted successfully!",
    });
    setTimeout(() => {
      GlobalState.set({
        openAlert: false,
      });
    }, 4000);
  };

  // Finishing a task. Called when the checkbox has been clicked
  // It changes the 'done' property of the task to true and updates the global state
  // In addition shows success alert
  let finishTask = (e) => {
    let tasksUpdated = [...tasks].filter((todo) => {
      if (todo.name === e.name) {
        todo.done = true;
      }
      return todo;
    });

    GlobalState.set({
      tasks: tasksUpdated,
      currentTask: "",
      openAlert: true,
      typeAlert: "success",
      textAlert: "Task is finished! Good job!",
    });

    // Close the alert after 2 seconds
    setTimeout(() => {
      GlobalState.set({
        openAlert: false,
      });
    }, 2000);
  };

  // This handles the tab change
  let handleTabChange = (e, value) => {
    GlobalState.set({
      currentTab: value,
    });
  };

  return (
    <>
      <Collapse in={openAlert}>
        <Alert severity={typeAlert}>{textAlert}</Alert>
      </Collapse>
      <Container>
        <TabContext value={currentTab} className='tab-container'>
          <TabList onChange={handleTabChange} centered>
            <Tab label='To Do' value='todo' />
            <Tab label='Done' value='done' />
          </TabList>
          <TabPanel value={"todo"} className='tab-panel'>
            <List sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}>
              {tasks
                .filter((x) => x.done === false)
                .map((task, index) => {
                  return <Task key={index} index={index} task={task} finishTask={finishTask} editTask={editTask} deleteTask={deleteTask} />;
                })}
            </List>
          </TabPanel>
          <TabPanel value={"done"} className='tab-panel'>
            {tasks.filter((x) => x.done === true).length === 0 ? (
              <p className='no-tasks'>No finished tasks.</p>
            ) : (
              <List sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}>
                {tasks
                  .filter((x) => x.done === true)
                  .map((task, index) => {
                    const labelId = `checkbox-list-label-${task}`;
                    return <Task key={index} index={index} task={task} finishTask={finishTask} labelId={labelId} />;
                  })}
              </List>
            )}
          </TabPanel>
        </TabContext>
      </Container>
      <InputWrap currentTask={currentTask} editTaskName={editTaskName} addTask={addTask} />
    </>
  );
}

export default Todo;
