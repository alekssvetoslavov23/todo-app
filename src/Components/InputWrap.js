import React from "react";
import { GlobalState } from "../Global";
import { Button, Input, Container } from "@mui/material";
import "../Styles/App.css";

class InputWrap extends React.Component {
  render() {
    const { addTask, currentTask, editTaskName } = this.props;

    // This is called on key press. If it's enter, automatically add the task
    let handleKeypress = (e) => {
      if (e.key === "Enter") {
        addTask(currentTask);
      }
    };

    // This is called everytime the input has been changed. The value is set to the global state
    let handleInputChange = (e) => {
      GlobalState.set({
        currentTask: e.target.value,
      });
    };

    return (
      <>
        <Container className='todo-list-inputs'>
          <Input
            placeholder='What you will do today?'
            onChange={handleInputChange}
            onKeyPress={handleKeypress}
            className='todo-input'
            value={currentTask}
          />
          <Button
            variant='contained'
            className='todo-button'
            onClick={(e) => {
              addTask(currentTask);
            }}>
            {editTaskName !== "" ? "Edit" : "Add"}
          </Button>
        </Container>
      </>
    );
  }
}

export default InputWrap;
