import React from "react";
import image from "../Assets/white_circle.png";

class ParallaxCircles extends React.Component {
  render() {
    return (
      <>
        <img src={image} alt='' className='layer-1' />
        <img src={image} alt='' className='layer-3' />
      </>
    );
  }
}

export default ParallaxCircles;
