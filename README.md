# TODO App

This README provides short description and information about the TODO list application.

### What is this repository for?

- The application stored in this repository is a to-do list. You can add tasks and mark them as finished. In addition, you can edit and delete them.
- Version 1.0.0

### How do I get set up?

- To install all important dependencies, first run:
- - npm install
- Then to start it with
- - npm start
- Now the application should be accessible at http://localhost:3000

### How to use it

- In order to add a task, you need to write something in the input. If it is empty it will throw error. And you can't add 2 tasks with the same text!
- To mark a task as completed, you must press the checkbox. Notification will show you that you marked the task as done successfully.
- To find the finished tasks, please locate to the "Done" tab.
- To edit a task, you press the edit button next to it, type something else in the input and click the "Edit" button
- To delete a task, you just press the delete icon, next to the edit one. You can't undo that action.

### How much time is spent creating the app

- It took me approximately 6 to 8 hours to develop the application. I spent more time on the design, the MUI components and testing all different combination of actions. First I started writing on one file only, but then I divided it into separate components, which also took time. At the end I tested everything once more and cleaned all files, including removing of all console warnings.
- If I didn't spent that much time on the visual part and just apply the logic, it won't take more than 3 hours.

### Who do I talk to?

- Creator: Aleksandar Svetoslavov
- Email: a.svetoslavov@student.fontys.nl
